//
//  AppDelegate.h
//  p02-muratore
//
//  Created by Anthony on 2/7/17.
//  Copyright © 2017 Anthony. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

