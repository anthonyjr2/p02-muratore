//
//  ViewController.h
//  p02-muratore
//
//  Created by Anthony on 2/7/17.
//  Copyright © 2017 Anthony. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *num1;
@property (weak, nonatomic) IBOutlet UILabel *num2;
@property (weak, nonatomic) IBOutlet UILabel *num3;
@property (weak, nonatomic) IBOutlet UILabel *num4;
@property (weak, nonatomic) IBOutlet UILabel *num5;
@property (weak, nonatomic) IBOutlet UILabel *num6;
@property (weak, nonatomic) IBOutlet UILabel *num7;
@property (weak, nonatomic) IBOutlet UILabel *num8;
@property (weak, nonatomic) IBOutlet UILabel *num9;
@property (weak, nonatomic) IBOutlet UILabel *num10;
@property (weak, nonatomic) IBOutlet UILabel *num11;
@property (weak, nonatomic) IBOutlet UILabel *num12;
@property (weak, nonatomic) IBOutlet UILabel *num13;
@property (weak, nonatomic) IBOutlet UILabel *num14;
@property (weak, nonatomic) IBOutlet UILabel *num15;
@property (weak, nonatomic) IBOutlet UILabel *num16;
@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *numArray;
@property (nonatomic, strong) NSMutableArray *numMatrix;
@property (nonatomic, strong) NSMutableArray *numData;

- (IBAction)upButton:(id)sender;
- (IBAction)downButton:(id)sender;
- (IBAction)leftButton:(id)sender;
- (IBAction)rightButton:(id)sender;

-(void) resetBoard;
-(void) createTile;
-(bool) tilesMovable;
-(void) showData;


@end

