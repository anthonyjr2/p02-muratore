//
//  main.m
//  p02-muratore
//
//  Created by Anthony on 2/7/17.
//  Copyright © 2017 Anthony. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
