//
//  ViewController.m
//  p02-muratore
//
//  Created by Anthony on 2/7/17.
//  Copyright © 2017 Anthony. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _numMatrix = [[NSMutableArray alloc] initWithCapacity:4];
    [_numMatrix insertObject:[NSMutableArray arrayWithObjects: _num1, _num2, _num3, _num4, nil] atIndex:0];
    [_numMatrix insertObject:[NSMutableArray arrayWithObjects: _num5, _num6, _num7, _num8, nil] atIndex:1];
    [_numMatrix insertObject:[NSMutableArray arrayWithObjects: _num9, _num10, _num11, _num12, nil] atIndex:2];
    [_numMatrix insertObject:[NSMutableArray arrayWithObjects: _num13, _num14, _num15, _num16, nil] atIndex:3];
    [self resetBoard];
    [self createTile];
    [self showData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)upButton:(id)sender
{
    int dx, dy, start_x, start_y;
    dx = 0;
    dy = -1;
    start_y = 0;
    start_x = 0;
    
    int end_x = (start_x == 3) ? -1 : 4;
    int end_y = (start_y == 3) ? -1 : 4;
    int loop_x = (start_x < end_x) ? 1 : -1;
    int loop_y = (start_y < end_y) ? 1 : -1;
    NSInteger x, y;
    
    int moves;
    bool didMove = false;
    bool win = false;
    
    for(moves = 0; moves < 10; moves++)
    {
        for(y = start_y; y != end_y; y+=loop_y)
        {
            if(y + dy > 3 || (y + dy < 0))
            {
                continue;
            }
            for(x = start_x; x != end_x; x+=loop_x)
            {
                if((x + dx > 3) || (x + dx < 0))
                {
                    continue;
                }
                id currentObj = _numData[y][x];
                id nextObj = _numData[y+dy][x+dx];
                
                int currentNum = [currentObj intValue];
                int nextNum = [nextObj intValue];
                
                if(nextNum == currentNum || nextNum == 0)
                {
                    nextNum = currentNum + nextNum;
                    currentNum = 0;
                    didMove = true;
                    if(nextNum == 2048)
                    {
                        win = true;
                    }
                }
                _numData[y][x] = [NSNumber numberWithInt:currentNum];
                _numData[y+dy][x+dx] = [NSNumber numberWithInt:nextNum];
                
                
            }
        }
    }
    
    if(didMove)
    {
        [self createTile];
        [self showData];
    }
    
    if(win)
    {
        //[self gameWin];
        NSLog(@"win the game!");
    }
    else if(![self tilesMovable])
    {
        //[self gameLose];
        NSLog(@"lose the game!");
    }
    
}

- (IBAction)downButton:(id)sender
{
    int dx, dy, start_x, start_y;
    dx = 0;
    dy = 1;
    start_y = 3;
    start_x = 0;
    
    int end_x = (start_x == 3) ? -1 : 4;
    int end_y = (start_y == 3) ? -1 : 4;
    int loop_x = (start_x < end_x) ? 1 : -1;
    int loop_y = (start_y < end_y) ? 1 : -1;
    NSInteger x, y;
    
    int moves;
    bool didMove = false;
    bool win = false;
    
    for(moves = 0; moves < 10; moves++)
    {
        for(y = start_y; y != end_y; y+=loop_y)
        {
            if(y + dy > 3 || (y + dy < 0))
            {
                continue;
            }
            for(x = start_x; x != end_x; x+=loop_x)
            {
                if((x + dx > 3) || (x + dx < 0))
                {
                    continue;
                }
                id currentObj = _numData[y][x];
                id nextObj = _numData[y+dy][x+dx];
                
                int currentNum = [currentObj intValue];
                int nextNum = [nextObj intValue];
                
                if(nextNum == currentNum || nextNum == 0)
                {
                    nextNum = currentNum + nextNum;
                    currentNum = 0;
                    didMove = true;
                    if(nextNum == 2048)
                    {
                        win = true;
                    }
                }
                _numData[y][x] = [NSNumber numberWithInt:currentNum];
                _numData[y+dy][x+dx] = [NSNumber numberWithInt:nextNum];
                
                
            }
        }
    }
    
    if(didMove)
    {
        [self createTile];
        [self showData];
    }
    
    if(win)
    {
        //[self gameWin];
        NSLog(@"win the game!");
    }
    else if(![self tilesMovable])
    {
        //[self gameLose];
        NSLog(@"lose the game!");
    }
}

- (IBAction)leftButton:(id)sender
{
    int dx, dy, start_x, start_y;
    dx = -1;
    dy = 0;
    start_y = 0;
    start_x = 0;
    
    int end_x = (start_x == 3) ? -1 : 4;
    int end_y = (start_y == 3) ? -1 : 4;
    int loop_x = (start_x < end_x) ? 1 : -1;
    int loop_y = (start_y < end_y) ? 1 : -1;
    NSInteger x, y;
    
    int moves;
    bool didMove = false;
    bool win = false;
    
    for(moves = 0; moves < 10; moves++)
    {
        for(y = start_y; y != end_y; y+=loop_y)
        {
            if(y + dy > 3 || (y + dy < 0))
            {
                continue;
            }
            for(x = start_x; x != end_x; x+=loop_x)
            {
                if((x + dx > 3) || (x + dx < 0))
                {
                    continue;
                }
                id currentObj = _numData[y][x];
                id nextObj = _numData[y+dy][x+dx];
                
                int currentNum = [currentObj intValue];
                int nextNum = [nextObj intValue];
                
                if(nextNum == currentNum || nextNum == 0)
                {
                    nextNum = currentNum + nextNum;
                    currentNum = 0;
                    didMove = true;
                    if(nextNum == 2048)
                    {
                        win = true;
                    }
                }
                _numData[y][x] = [NSNumber numberWithInt:currentNum];
                _numData[y+dy][x+dx] = [NSNumber numberWithInt:nextNum];
                
                
            }
        }
    }
    
    if(didMove)
    {
        [self createTile];
        [self showData];
    }
    
    if(win)
    {
        //[self gameWin];
        NSLog(@"win the game!");
    }
    else if(![self tilesMovable])
    {
        //[self gameLose];
        NSLog(@"lose the game!");
    }
}

- (IBAction)rightButton:(id)sender
{
    int dx, dy, start_x, start_y;
    dx = 1;
    dy = 0;
    start_y = 0;
    start_x = 3;
    
    int end_x = (start_x == 3) ? -1 : 4;
    int end_y = (start_y == 3) ? -1 : 4;
    int loop_x = (start_x < end_x) ? 1 : -1;
    int loop_y = (start_y < end_y) ? 1 : -1;
    NSInteger x, y;
    
    int moves;
    bool didMove = false;
    bool win = false;
    
    for(moves = 0; moves < 10; moves++)
    {
        for(y = start_y; y != end_y; y+=loop_y)
        {
            if(y + dy > 3 || (y + dy < 0))
            {
                continue;
            }
            for(x = start_x; x != end_x; x+=loop_x)
            {
                if((x + dx > 3) || (x + dx < 0))
                {
                    continue;
                }
                id currentObj = _numData[y][x];
                id nextObj = _numData[y+dy][x+dx];
                
                int currentNum = [currentObj intValue];
                int nextNum = [nextObj intValue];
                
                if(nextNum == currentNum || nextNum == 0)
                {
                    nextNum = currentNum + nextNum;
                    currentNum = 0;
                    didMove = true;
                    if(nextNum == 2048)
                    {
                        win = true;
                    }
                }
                _numData[y][x] = [NSNumber numberWithInt:currentNum];
                _numData[y+dy][x+dx] = [NSNumber numberWithInt:nextNum];
                
                
            }
        }
    }
    
    if(didMove)
    {
        [self createTile];
        [self showData];
    }
    
    if(win)
    {
        //[self gameWin];
        NSLog(@"win the game!");
    }
    else if(![self tilesMovable])
    {
        //[self gameLose];
        NSLog(@"lose the game!");
    }
}

- (void)resetBoard
{
    _numData = [[NSMutableArray alloc] initWithCapacity:4];
    [_numData insertObject:[NSMutableArray arrayWithObjects: @0, @0, @0, @0, nil] atIndex:0];
    [_numData insertObject:[NSMutableArray arrayWithObjects: @0, @0, @0, @0, nil] atIndex:1];
    [_numData insertObject:[NSMutableArray arrayWithObjects: @0, @0, @0, @0, nil] atIndex:2];
    [_numData insertObject:[NSMutableArray arrayWithObjects: @0, @0, @0, @0, nil] atIndex:3];
}

- (void)createTile
{
    int randX = arc4random_uniform(4);
    int randY = arc4random_uniform(4);
    
    while(![_numData[randY][randX] isEqual: @0])
    {
        randX = arc4random_uniform(4);
        randY = arc4random_uniform(4);
    }
    //((UILabel*)[_numArray objectAtIndex:x]).text = [NSString stringWithFormat:@"%@",@2];
    _numData[randY][randX] = @2;
}


- (bool)tilesMovable
{
    NSInteger x, y;
    for(x = 0; x < 4; x++)
    {
        for(y = 0; y < 4; y++)
        {
            if([_numData[y][x] isEqual: @0])
            {
                return true;
            }
            if(x > 0 && [_numData[y][x-1] isEqual:_numData[y][x]])
            {
                return true;
            }
            if(x < 3 && [_numData[y][x+1] isEqual:_numData[y][x]])
            {
                return true;
            }
            if(y > 0 && [_numData[y-1][x] isEqual:_numData[y][x]])
            {
                return true;
            }
            if(y < 3 && [_numData[y+1][x] isEqual:_numData[y][x]])
            {
                return true;
            }
        }
    }
    return false;
}

-(void)showData
{
    NSInteger x, y;
    for(x=0; x<4; x++) {
        for(y=0; y<4; y++) {
            UILabel* box = [[_numMatrix objectAtIndex: x] objectAtIndex: y];
            int val = [[[_numData objectAtIndex: x] objectAtIndex: y] intValue];
            if(val == 0) {
                [box setText: @""];
            }
            else {
                [box setText: [NSString stringWithFormat: @"%d", val]];
                int rgb = 0x333300;
                if(val == 2) {
                    rgb = 0x333333;
                }
                else if(val == 4) {
                    rgb = 0x333366;
                }
                else if(val == 8) {
                    rgb = 0x333399;
                }
                else if(val == 16) {
                    rgb = 0x3333CC;
                }
                else if(val == 32) {
                    rgb = 0x3333FF;
                }
                else if(val == 64) {
                    rgb = 0x3366FF;
                }
                else if(val == 128) {
                    rgb = 0x3399FF;
                }
                else if(val == 256) {
                    rgb = 0x6699FF;
                }
                else if(val == 512) {
                    rgb = 0x9999FF;
                }
                else if(val == 1024) {
                    rgb = 0xCC99FF;
                }
                else if(val == 2048) {
                    rgb = 0xFF99FF;
                }
                //box.textColor = UIColorFromRGB(rgb);
            }
        }
    }
}

@end
